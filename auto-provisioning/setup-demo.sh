#!/bin/bash

if [ $# != 3 ]; then
  echo "create <num-of-users> on the given Nextcloud instance."
  echo "All user are called userN"
  echo ""
  echo "call $0 <num-of-users> <nextcloud-url> <admin-password>"
  exit
fi

NUM_OF_USERS=$1
# strip trailing slash
NEXTCLOUD=${2%/}
PWD=$3

USERNAME='user'
ADMIN='admin'

INSTANCE_NAME=${NEXTCLOUD##*/}

COUNT=0
RESULT=''

echo "Install Talk..."
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ app:install spreed
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ talk:signaling:add --verify $HPB_URL $HPB_PWD
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ talk:turn:add --secret $TURN_SECRET -- $TURN "udp,tcp"

echo "Install Collabora..."
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ app:install richdocuments
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ config:app:set --value $COLLABORA richdocuments public_wopi_url
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ config:app:set --value $COLLABORA richdocuments wopi_url
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ richdocuments:activate-config

echo "Install Deck..."
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ app:install deck

echo "Install Calendar..."
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ app:install calendar

echo "Install Mail..."
ssh root@try.nextcloud.com sudo -u www-data php /mnt/demos/$INSTANCE_NAME/occ app:install mail

# Create user accounts
for (( c=0; c<$NUM_OF_USERS; c++ ))
do
        echo -n "create \"$USERNAME$c\" - Result:"
        PASSWORD="$(pwgen 8 1)"
        REQUEST="curl -s -X POST -u $ADMIN:$PWD $NEXTCLOUD/ocs/v1.php/cloud/users -d userid=\"$USERNAME$c\" -d password=\"$PASSWORD\" -H OCS-APIRequest:true | grep message | sed -e 's/<[^>]*>//g'"
        eval $REQUEST
        RESULT="${RESULT}\nusername: $USERNAME$c password: $PASSWORD"
done

echo -e "\n\n"
echo "Created Users"
echo "============="
echo -e $RESULT
echo ""
echo "URL: $NEXTCLOUD"
